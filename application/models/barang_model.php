<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class barang_model extends CI_Model
{
	//panggil nama table
	private $_table = "barang";

public function rules()
{
		return[
		[
				'field' =>'kode_barang',
				'label' =>'Kode Barang',
				'rules' =>'required|max_length[5]',
				'errors' =>[
					'required' => 'kode barang tidak boleh kosong.',
					'max_length' => 'kode barang tidak boleh lebih dari 5 karakter.',
				]

		],
		[
				'field' =>'nama_barang',
				'label' =>'Nama Barang',
				'rules' =>'required',
				'errors' =>[
					'required' => 'nama barang tidak boleh kosong.',
					
				]

		],
		[
				'field' =>'harga_barang',
				'label' =>'Harga Barang',
				'rules' =>'required|numeric',
				'errors' =>[
					'required' => 'harga barang tidak boleh kosong.',
					'numeric' => 'harga barang harus angka.',
				]
		],
		[
				'field' =>'kode_jenis',
				'label' =>'Kode Jenis',
				'rules' =>'required',
				'errors' =>[
					'required' => 'kode barang tidak boleh kosong.',
					
				]
		],
		[
				'field' =>'stok',
				'label' =>'Stok Barang',
				'rules' =>'required|numeric',
				'errors' =>[
					'required' => 'stok barang tidak boleh kosong.',
					'numeric' => 'stok barang harus angka.',
		]
		]
];
}
	


	
	public function tampilDataBarang()
	
	{
		//seperti : select * from <name_table>
		return $this->db->get($this->_table)->result();
	}
	
	public function tampilDataBarang2()
	
	{
		$query = $this->db->query("select * from jenis_barang as jb inner join barang as br on jb.kode_jenis=br.kode_jenis");
		return $query->result();
	}
	
	public function tampilDataBarang3()
	
	{
		$this->db->select('*');
		$this->db->order_by('kode_barang', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	public function save()
	
	{
		
		
		$data['kode_barang'] =$this->input->post('kode_barang');
		$data['nama_barang'] =$this->input->post('nama_barang');
		$data['harga_barang'] =$this->input->post('harga_barang');
		$data['kode_jenis'] =$this->input->post('kode_jenis');
		
		$data['flag'] =1;
		$this->db->insert($this->_table, $data);
		
		}
		
	public function detail($kode_barang)
	
	{
		$this->db->select('*');
		$this->db->where('kode_barang', $kode_barang);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	
	public function update($kode_barang)
	
	{
		$data['kode_barang'] =$this->input->post('kode_barang');
		$data['nama_barang'] =$this->input->post('nama_barang');
		$data['harga_barang'] =$this->input->post('harga_barang');
		$data['kode_jenis'] =$this->input->post('kode_jenis');
		$data['stok'] =$this->input->post('stok');
		
		$data['flag'] =1;
		
		$this->db->where('kode_barang', $kode_barang);
		$this->db->update($this->_table, $data);

	}
	
	public function delete($kode_barang)
	{
		$this->db->where('kode_barang', $kode_barang);
		$this->db->delete($this->_table);	
	}
	
	
	
	public function updateStok($kode_barang, $qty)
	{
		$cari_stok = $this->detail($kode_barang);
		foreach ($cari_stok as $data ){
			$stok = $data->stok;
	}
		
		$jumlah_stok = $stok + $qty;
		$data_barang['stok'] = $jumlah_stok;
		
		$this->db->where('kode_barang', $kode_barang);
		$this->db->update('barang', $data_barang);
	}
}
