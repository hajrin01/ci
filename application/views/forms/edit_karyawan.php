
<table width="40%" height="70" border="0" align="center">
  <center><h2 style="font-family:'Comic Sans MS', cursive">Edit Karyawan</h2></center>
  
  <?php
foreach ($detail_karyawan as $data) {
	$nik  = $data->nik;
	$nama_lengkap  = $data->nama_lengkap;
	$tempat_lahir  = $data->tempat_lahir;
	$tgl_lahir  = $data->tgl_lahir;
	$jenis_kelamin  = $data->jenis_kelamin;
	$alamat  = $data->alamat;
	$telp  = $data->telp;
	$kode_jabatan  = $data->kode_jabatan;
}

$thn_pisah = substr($tgl_lahir, 0, 4);
$bln_pisah = substr($tgl_lahir, 5, 2);
$tgl_pisah = substr($tgl_lahir, 8, 2);
?>
<center><div style="color: red"><?= validation_errors(); ?></div></center>
<form action="<?=base_url()?>karyawan/editkaryawan/<?= $nik; ?>" method="POST">
<table width="40%" border="0" cellspacing="0" cellpadding="5" align="center">
  <tr>
    <br />  
    <td>Nik</td>
    <td>:</td>
    <td>
      <input type="text" name="nik" id="nik" maxlength="10" value ="<?=$nik;?>" value="<?= set_value('nik');?>" readonly /></td>
  </tr>
  <tr>
    <td>Nama Karyawan</td>
    <td>:</td>
    <td>
      <input type="text" name="nama_lengkap" id="nama_lengkap" value ="<?=$nama_lengkap;?>" value="<?= set_value('nama_lengkap');?>" /></td>
  </tr>
  <tr>
    <td>Tempat Lahir</td>
    <td>:</td>
    <td> <input type="text" name="tempat_lahir" id="tempat_lahir" value ="<?=$tempat_lahir;?>"  value="<?= set_value('tempat_lahir');?>" /></td>
    </td>
  </tr>
  <tr>
    <td height="35">Jenis Kelamin</td>
    <td>:</td>
    <td><?php
    	if($jenis_kelamin == 'P'){
			 $slc_P = 'SELECTED';
			 $slc_L = '';
		}elseif($jenis_kelamin == 'L'){
			 $slc_L = 'SELECTED';
			 $slc_P = '';
		}else{
			 $slc_P = '';
			 $slc_L = '';
		}
	?>
    <label for="jenis_kelamin"></label>
      <select name="jenis_kelamin" id="jenis_kelamin" value="<?= set_value('jenis_kelamin');?>" />
      <option <?=$slc_P;?> value="P">Perempuan</option>
      <option <?=$slc_L;?> value="L">Laki-Laki</option>
      </select>
    </td>
  </tr>
  <tr>
    <td>Tanggal Lahir</td>
    <td>:</td>
    <td><select name="tgl" id="tgl">
    <?php
	for($tgl=1;$tgl<=31;$tgl++){
		$select_tgl = ($tgl == $tgl_pisah) ? 'selected' : '';
		?>
        <option value="<?=$tgl;?>" <?=$select_tgl; ?>><?=$tgl;?></option>
        <?php
	}
	?>
    </select>
      <select name="bln" id="bln">
       <?php
	   $bln_n = array('januari','februari','maret','april','mei','juni','juli','agustus','september','oktober','november','desember');
	for($bln=0;$bln<12;$bln++){
		$select_bln = ($bln == $bln_pisah) ? 'selected' : '';
		?>
        <option value="<?=$bln+1;?>" <?=$select_bln; ?>>
		<?=$bln_n[$bln];?>
        </option>
        <?php
	}
	?>
      </select>
      <select name="thn" id="thn">
       <?php
	for($thn=date('Y')-60;$thn<=date('Y')-15;$thn++){
		$select_thn = ($thn == $thn_pisah) ? 'selected' : '';
		?>
        <option value="<?=$thn;?>" <?=$select_thn; ?>><?=$thn;?></option>
        <?php
	}
	?>
      </select>
      </td>
  </tr>
  <tr>
    <td>Telepon</td>
    <td>:</td>
    <td><input type="text" name="telp" id="telp" value ="<?=$telp;?>"  value="<?= set_value('telp');?>" /></td>
    </td>
  </tr>
 
  <tr>
    <td>Alamat</td>
    <td>:</td>
    <td><textarea name="alamat" id="alamat" cols="45" rows="5" value="<?= set_value('alamat');?>" /><?=$alamat;?></textarea></td>
  </tr>
 <tr>
    <td height="35">Jabatan</td>
    <td>:</td>
    <td><select name="kode_jabatan" id="kode_jabatan" value ="<?=$kode_jabatan ;?>" >
    <?php foreach($data_jabatan as $data) {
		$select_jabatan = ($data->kode_jabatan == $kode_jabatan) ? 'selected' : '';
		
		?>
       <option value="<?= $data->kode_jabatan;?>" <?=$select_jabatan; ?>>
       <?= $data->nama_jabatan; ?></option>
       
      
      <?php }?>
      
    </select>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><input type="submit" name="simpan" id="simpan" value="simpan">
      <input type="submit" name="batal" id="batal" value="reset">
      <br></br>
      <a href="<?=base_url();?>karyawan/listkaryawan"><input type="button" name="kembali ke menu sebelumnya" id="kembali ke menu sebelumnya" value="kembali ke menu sebelumnya"></a></td>
  </tr>
</table>
</table>
</form>
</body>
