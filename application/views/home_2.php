<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	$user_login = $this->session->userdata();
 ?>
  
 <head><title>CI_TOKO</title>
 
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/style.css">
 </head>
 <body>
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
   
  
   <center>
<?php 
	if ($user_login['tipe']== "1"){
		$this->load->view('template/view_menu');
	}else{
		$this->load->view('template/view_menu_user');
	}
	
?>

</center>

<?php $this->load->view($content);?>
</body>